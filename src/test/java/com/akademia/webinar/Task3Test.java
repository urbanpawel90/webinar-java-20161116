package com.akademia.webinar;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Task3Test {
    @Test
    public void testExampleCase1() {
        executeTestCase(new int[]{1, 3, -3}, 6);
    }

    @Test
    public void testExampleCase2() {
        executeTestCase(new int[]{-8, 4, 0, 5, -3, 6}, 14);
    }

    private void executeTestCase(int[] ints, int expected) {
        Task3 task = new Task3();
        int result = task.solution(ints);

        assertEquals(result, expected);
    }
}
