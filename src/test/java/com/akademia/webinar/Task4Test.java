package com.akademia.webinar;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Task4Test {
    @Test
    public void testExampleCase() {
        executeTestCase(new int[]{9, 4, -3, -10}, 3);
    }

    @Test
    public void testEmptyArray() {
        executeTestCase(new int[0], 0);
    }

    private void executeTestCase(int[] arr, int expected) {
        Task4 task = new Task4();
        int result = task.solution(arr);

        assertEquals(result, expected);
    }
}
