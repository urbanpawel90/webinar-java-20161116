package com.akademia.webinar;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Task5Test {
    @Test
    public void testExampleCase() {
        executeTestCase(new int[]{1, -1, 0, 2, 3, 5}, 3, 2);
    }

    private void executeTestCase(int[] ints, int d, int expected) {
        Task5 task = new Task5();
        int result = task.solution(ints, d);

        assertEquals(result, expected);
    }
}
