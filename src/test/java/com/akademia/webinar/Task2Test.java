package com.akademia.webinar;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Task2Test {
    @Test
    public void testExampleCase() {
        executeTestCase("(())))(", 4);
    }

    @Test
    public void testSimplestCustomCase() {
        executeTestCase("()", 1);
        executeTestCase("", 0);
        executeTestCase("(", 0);
        executeTestCase(")", 1);
        executeTestCase("())", 2);
        executeTestCase("(()", 1);
        executeTestCase("()(()))", 4);

        // Pawel's case
        executeTestCase("))())(()", 5);
    }

    private void executeTestCase(String source, int expected) {
        Task2 task = new Task2();
        int result = task.solution(source);

        assertEquals(expected, result);
    }
}
