package com.akademia.webinar;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Task1Test {
    @Test
    public void testCase1() {
        testGivenCase("00-44  48 5555 8361", "004-448-555-583-61");
    }

    @Test
    public void testCase2() {
        testGivenCase("0 - 22 1985--324", "022-198-53-24");
    }

    @Test
    public void testCase3() {
        testGivenCase("555372654", "555-372-654");
    }

    @Test
    public void testCase4() {
        testGivenCase("1-2", "12");
        testGivenCase("12", "12");
    }

    @Test
    public void testCase5() {
        testGivenCase("123", "123");
        testGivenCase("1  2 3", "123");
    }

    @Test
    public void testCase6() {
        testGivenCase("1256", "12-56");
    }

    private void testGivenCase(String given, String expected) {
        Task1 task = new Task1();
        String result = task.solution(given);

        assertEquals(result, expected);
    }
}
