package com.akademia.webinar;

public class Task1 {
    public String solution(String s) {
        // Remove spaces and dashes
        s = s.replaceAll("\\D", "");

        StringBuilder result = new StringBuilder();

        // How many digits should be in section
        int offset = 3;
        for (int i = 0; i < s.length(); i += offset) {
            // If there is 4 digits left then change section size to 2
            if (s.length() - i == 4) {
                offset = 2;
            }

            // Append current section
            result.append(s.substring(i, Math.min(s.length(), i + offset)));
            // If it's not last section append dash
            if (i + offset < s.length()) {
                result.append("-");
            }
        }

        return result.toString();
    }
}
