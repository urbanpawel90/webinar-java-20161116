package com.akademia.webinar;

public class Task2 {

    private static final int DIRECTION_FORWARD = 1;
    private static final int DIRECTION_BACKWARD = -1;

    public int solution(String s) {
        // We are interested in calculating anything when string is empty
        if (s.length() == 0) {
            return 0;
        }

        // result - initially we hold a middle index of the string
        // direction - value we should add to result each iteration (1 or -1)
        // we do it until we reach after last char of before first char
        for (int result = s.length() / 2, direction = DIRECTION_FORWARD;
             result <= s.length() && result >= 0;
             result += direction) {
            int openingBrackets = countChar(s, '(', 0, result);
            int closingBrackets = countChar(s, ')', result, s.length());

            // When opening and closing brackets counts are equals we returning result
            if (openingBrackets == closingBrackets) {
                return result;
            } else if (openingBrackets > closingBrackets) {
                // If we have more opening brackets than closing ones we should change direction to backward
                direction = DIRECTION_BACKWARD;
            }
        }

        return s.length();
    }

    private int countChar(String source, char needle, int start, int end) {
        int cCount = 0;
        source = source.substring(start, end);
        for (int i = 0; i < source.length(); i++) {
            if (source.charAt(i) == needle) {
                cCount++;
            }
        }
        return cCount;
    }
}
