package com.akademia.webinar;

public class Task4 {
    public int solution(int[] arr) {
        double mean = countMean(arr);
        int maxIndex = 0;
        double maxDev = arr.length == 0 ? 0 : Math.abs(arr[0] - mean);

        for (int i = 0; i < arr.length; i++) {
            double deviation = Math.abs(arr[i] - mean);
            if (deviation > maxDev) {
                maxDev = deviation;
                maxIndex = i;
            }
        }
        return maxIndex;
    }

    private double countMean(int[] arr) {
        int sum = 0;
        for (int i : arr) {
            sum += i;
        }
        return (double) sum / arr.length;
    }
}
