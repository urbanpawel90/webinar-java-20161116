package com.akademia.webinar;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Created by pawelurban on 16.11.2016.
 */
public class Main {
    static final String NAME = "Pawel";

    public static void main(String[] args) {
        try (InputStream is = System.in) {
            new Scanner(is).next();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public enum Gender {
        MAN, WOMAN
    }
}
