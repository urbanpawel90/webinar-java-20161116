package com.akademia.webinar;

public class Task3 {
    public int solution(int[] a) {
        // Because we know that a has at least 1 element we can take first pair (0, 0) as the first max value
        int max = sumDistance(a, 0, 0);

        // Q is from 0 to N-1
        for (int q = 0; q < a.length; q++) {
            // P is from 0 to Q
            for (int p = 0; p <= q; p++) {
                int sumDistance = sumDistance(a, p, q);
                // If sumDistance for (P,Q) combination is greater than max then we replace max value
                if (sumDistance > max) {
                    max = sumDistance;
                }
            }
        }

        return max;
    }

    private int sumDistance(int[] a, int p, int q) {
        return a[p] + a[q] + (q - p);
    }
}
